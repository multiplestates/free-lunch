 <!DOCTYPE html> 

<html lang="en">

  <head>
  <meta charset="UTF-8">

  <!--The below makes sure the latest version of IE is used in versions of IE that contain multiple rendering engines but it break w3c validation. If you don't want to break validation then add it to your .htaccess file. src - https://github.com/h5bp/html5-boilerplate/blob/v4.3.0/doc/html.md -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title><?php echo $page_title ?></title>
  
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <?php include('functions.php') ?>
  
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="<?php echo $site_url ?>/your-icon.png">
  <link rel="apple-touch-icon" sizes="196x196" href="<?php echo $site_url ?>/your-icon.png">
  
  <link rel="stylesheet" href="<?php echo $site_url ?>/css/style.min.css" >

  </head>
  <body>
    
    <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <div class="l-page-holder page-holder">
    
    <div class="l-strip">
      <header>
        <div class="l-inner">
          <div class="the-head">
            <h1 class="the-head-title">PHP-starter-template</h1>
          </div>
        </div>
      </header>
    </div>
