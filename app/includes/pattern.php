<!-- Typography -->
<div class="pattern">
  <div class="display">
    <h1>Heading h1</h1>
    <h2>Heading h2</h2>
    <h3>Heading h3</h3>
    <h4>Heading h4</h4>
    <h5>Heading h5</h5>
    <h6>Heading h6</h6>
    <p>This is a paragraph of text. Some of the text may be <em>emphasised</em> and some it might even be <strong>strongly emphasised</strong>. Occasionally <q>quoted text</q> may be found within a paragraph &hellip;and of course <a href="#">a link</a> may appear at any point in the text. The average paragraph contains five or six sentences although some may contain as little or one or two while others carry on for anything up to ten sentences and beyond.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquip ex veniam, quis nostrud exercitation ullamco laboris nisi ut ea commodo consequat. In reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui mollit anim id est laborum.</p>
    <ol>
    <li>First list item</li>
    <li>Second item in a list of ordered items</li>
    <li>Third item in the list</li>
    </ol>
    <ul>
    <li>A list item</li>
    <li>Another item in a list</li>
    <li>Yet another item in this list of items</li>
    </ul>
  </div>
</div>

<!-- Blockquote -->
<div class="pattern">
  <div class="display">
    <blockquote>
    <p>This text is quoted. A block of quoted text like this is particularly useful when presented as a pull-quote within an article of text.</p>
    </blockquote>
  </div>
</div>

<!-- Feedback error -->
<div class="pattern">
  <div class="display">
    <div class="feedback error">
    <p>This is an error feedback message.</p>
    </div>
  </div>
</div>

<!-- Feedback -->
<div class="pattern">
  <div class="display">
    <div class="feedback">
    <p>This is a feedback message for the user.</p>
    </div>
  </div>
</div>

<!-- Buttons -->
<div class="pattern">
  <div class="display">
    <button>Button element</button>
    <input type="submit" value="Submit button">
    <a class="button" href="#">Link with button class</a>
    <a class="button button-light" href="#">Link with button class</a>
  </div>
</div>

<!-- Form Elements -->
<div class="pattern">
  <div class="display">
    <form id="your-contact-form" action="" method="post">
      <fieldset>
      <legend>This is your Legend</legend>
      <ol>
        <li>
          <label for=""><input type="checkbox" name="" id=""> Label text</label>
        </li>
      
        <li>
          <label for="">Label text</label>
          <input type="email"  name="" id="">
        </li>

        <li>
          <label for="">Label text</label>
          <input type="number"  name="" id="">
        </li>

        <li>
          <select>
          <option>option text</option>
          </select>
        </li>

        <li>
          <label for="">Label text</label>
          <input type="text"  name="" id="">
        </li>

        <li>
          <label for="">Label text</label>
          <textarea rows="5" cols="20" name="" id=""></textarea>
        </li>

        <li>
          <label for="">Label text</label>
          <input type="url"  name="" id="">
        </li>

        <li>
          <input type="submit" value="Submit button">
        </li>
      </ol>
      </fieldset>
    </form>
  </div>
</div>

