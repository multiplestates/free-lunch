<?php 
  // Set the name of the current page before including the header
  $page_title = 'Home';
  // Include the header
  include('header.php') 
?>

  <div class="l-strip">
    <div class="l-holder">
      <div class="l-inner">
        <div class="index-content"><p>Index content</p></div>
      </div>
    </div>
  </div>

<?php include('footer.php') ?>