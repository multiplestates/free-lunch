<div class="l-strip">
  <div class="l-holder">
    <div class="l-inner">
      
      <div class="post-preview">
        <h2 class="post-preview-title"><?php echo $postTitle ?></h2>
        <img clss="post-preview-img" src="<?php echo $postImg ?>" alt="<?php echo $postImgAlt ?>" >
        <p class="post-preview-content"><?php echo $postContent ?></p>
        <a href="<?php echo $postLink ?>" >Read more</a>
      </div>

    </div>
  </div>
</div>